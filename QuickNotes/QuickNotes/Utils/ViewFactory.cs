﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace QuickNotes
{
    public class ViewFactory
    {
		public static void Initialize()
		{
            map = new Dictionary<Type, Type>();
        }

        public static void Register<TViewModel, TView>()
            where TViewModel : BaseViewModel
            where TView : Page, new()
        {
            if (map == null)
            {
                throw new Exception("Error! ViewFactory is not initialized.");
            }

            map[typeof(TViewModel)] = typeof(TView);
        }

        public static Page CreatePage<TViewModel>() where TViewModel : BaseViewModel, new()
        {
            if (map == null)
            {
                throw new Exception("Error! ViewFatory is not initialized.");
            }

            var viewType = map[typeof(TViewModel)];
            var page = (Page)Activator.CreateInstance(viewType);
            var viewModel = Activator.CreateInstance<TViewModel>();

            viewModel.Navigation = page.Navigation;
            page.BindingContext = viewModel;

            return page;
        }

        public static Page CreatePage<TViewModel>(TViewModel viewModel) where TViewModel : BaseViewModel
        {
            if (map == null)
            {
                throw new Exception("Error! ViewFactory is not initialized.");
            }

            var viewType = map[typeof(TViewModel)];
            var page = (Page)Activator.CreateInstance(viewType);

            viewModel.Navigation = page.Navigation;
            page.BindingContext = viewModel;

            return page;
        }

        private static IDictionary<Type, Type> map;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QuickNotes
{
    public class BasePage : ContentPage
    {
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (BindingContext != null)
            {
                BaseViewModel viewModel = BindingContext as BaseViewModel;
                if (viewModel != null) {
                    viewModel.OnAppearing();
                }
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            if (BindingContext != null)
            {
                BaseViewModel viewModel = BindingContext as BaseViewModel;
                if (viewModel != null)
                {
                    viewModel.OnDisappearing();
                }
            }
        }

    }
}

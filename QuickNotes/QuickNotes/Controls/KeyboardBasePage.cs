﻿
using Xamarin.Forms;

//http://stackoverflow.com/questions/31172518/how-do-i-keep-the-keyboard-from-covering-my-ui-instead-of-resizing-it

namespace QuickNotes
{
    public class KeyboardBasePage : BasePage
    {
        public bool CancelsTouchesInView = true;
    }
}

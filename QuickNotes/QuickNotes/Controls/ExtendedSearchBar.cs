﻿using System.Windows.Input;
using Xamarin.Forms;

namespace QuickNotes
{
    public class ExtendedSearchBar : SearchBar
    {
        public ExtendedSearchBar()
        {
            this.TextChanged += OnTextChanged;
        }

        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue != null && this.TextChangedCommand != null && this.TextChangedCommand.CanExecute(e.NewTextValue))
            {
                TextChangedCommand.Execute(e.NewTextValue);
            }

            if (string.IsNullOrEmpty(e.NewTextValue))
            {
                Unfocus();
            }
        }

        public ICommand TextChangedCommand
        {
            get { return (ICommand)this.GetValue(TextChangedCommandProperty); }
            set { this.SetValue(TextChangedCommandProperty, value); }
        }

        public static BindableProperty TextChangedCommandProperty = BindableProperty.Create("TextChangedCommand", typeof(ICommand), typeof(ExtendedSearchBar), null);
    }
}

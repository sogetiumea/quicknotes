﻿using System;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;
using QuickNotes.Core;

namespace QuickNotes
{
    // You exclude the 'Extension' suffix when using in Xaml markup
    [ContentProperty("Text")]
    public class TranslateExtension : IMarkupExtension
    {
        public TranslateExtension()
        {
            translateHelper = ComponentContainer.Resolve<ITranslateHelper>();
        }

        public string Text { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Text == null)
                return "";

            return translateHelper.GetString(Text);
        }

        private ITranslateHelper translateHelper;
    }
}

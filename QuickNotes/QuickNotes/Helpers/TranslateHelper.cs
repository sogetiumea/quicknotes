﻿
using System.Resources;
using System.Reflection;
using System.Globalization;
using QuickNotes.Core;
using System.Diagnostics;

namespace QuickNotes
{
    public class TranslateHelper : ITranslateHelper
    {
        public TranslateHelper(ILocalizeHelper localizeHelper)
        {
            ci = localizeHelper.GetCurrentCultureInfo();
        }

        public string GetString(string key)
        {
            ResourceManager rm = new ResourceManager("QuickNotes.Resources.Strings", typeof(TranslateHelper).GetTypeInfo().Assembly);

            string result = rm.GetString(key, ci);

            if (result == null)
            {
                Debug.WriteLine("I18N Error: Could not find translation for key '" + key + "'");
                result = key;
            }

            return result;
        }

        private CultureInfo ci;
    }
}

﻿
using Xamarin.Forms;
using PropertyChanged;
using QuickNotes.Core;

namespace QuickNotes
{
    [ImplementPropertyChanged]
    public class BaseViewModel
    {
        public virtual void OnAppearing()
        {
        }

        public virtual void OnDisappearing()
        {
        }

        protected string Translate(string key)
        {
            return TranslateHelper.GetString(key);
        }

        private ITranslateHelper translateHelper;
        public ITranslateHelper TranslateHelper
        {
            get { return translateHelper ?? (translateHelper = ComponentContainer.Resolve<ITranslateHelper>()); }
        }

        private IMessageHelper messageHelper;
        public IMessageHelper MessageHelper
        {
            get { return messageHelper ?? (messageHelper = ComponentContainer.Resolve<IMessageHelper>()); }
        }


        public INavigation Navigation { get; set; }
    }
}

﻿using Acr.UserDialogs;
using QuickNotes.Core;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QuickNotes
{
    public class NoteListViewModel : BaseViewModel
    {
        public NoteListViewModel()
        {
            this.noteManager = ComponentContainer.Resolve<INoteManager>();

            MessageHelper.Subscribe<INoteManager>(this, Messages.NotesUpdatedMessage, (p) => NotesUpdatedMessage());

            Device.BeginInvokeOnMainThread(async () =>
            {
                await ReloadNoteList();
                await SyncNotes();
            });
        }

        private void NotesUpdatedMessage()
        {
            if (skipNextUpdateNotification)
            {
                skipNextUpdateNotification = false;
            }
            else
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await ReloadNoteList();
                });
            }
        }

        private async Task ReloadNoteList()
        {
            noteList = await noteManager.GetNotes();

            if (IsRefreshing)
            {
                IsRefreshing = false;
                if (Device.OS == TargetPlatform.iOS)
                {
                    await Task.Delay(400);
                }
            }

            UpdateFilteredNotes();
        }

        private void UpdateFilteredNotes()
        {
            if (string.IsNullOrWhiteSpace(filterText))
            {
                ItemList = new ObservableCollection<NoteListItem>(noteList.Select(n => new NoteListItem { Note = n }));
                return;
            }
            var lowerFilterText = filterText.Trim().ToLower();

            var filteredItems = noteList.Where(n => n.Title.ToLower().Contains(lowerFilterText))
                                        .Select(n => new NoteListItem { Note = n });

            ItemList = new ObservableCollection<NoteListItem>(filteredItems);
        }
    
        private async Task NoteSelected(Note note)
        {
            SelectedItem = null;
            EditNoteViewModel viewModel = new EditNoteViewModel(note);
            await Navigation.PushAsync(ViewFactory.CreatePage<EditNoteViewModel>(viewModel));
        }

        private void SearchTextChanged(object item)
        {
            string searchText = item as string;
            if (searchText == null)
            {
                return;
            }
            filterText = searchText;
            UpdateFilteredNotes();
        }

        private async Task NewNote()
        {
            await Navigation.PushAsync(ViewFactory.CreatePage<EditNoteViewModel>());
        }

        private async Task SyncNotes()
        {
            await noteManager.SyncNotes();
        }

        private async Task DeleteNote(NoteListItem noteListItem)
        {
            bool result = await UserDialogs.Instance.ConfirmAsync(Translate("DeleteNoteText"), Translate("DeleteNoteTitle"), Translate("GenOk"), Translate("GenCancel"));
            if (result)
            {
                ItemList.Remove(noteListItem);
                skipNextUpdateNotification = true;
                await noteManager.DeleteNote(noteListItem.Note);
                await noteManager.SyncNotes();
            }
        }

        private Command deleteNoteCommand;
        public Command DeleteNoteCommand
        {
            get
            {
                return deleteNoteCommand ?? (deleteNoteCommand = new Command<NoteListItem>(async (item) => await DeleteNote(item)));
            }
        }

        private Command syncNotesCommand;
        public Command SyncNotesCommand
        {
            get
            {
                return syncNotesCommand ?? (syncNotesCommand = new Command(async () => await SyncNotes()));
            }
        }

        private Command newNoteCommand;
        public Command NewNoteCommand
        {
            get
            {
                return newNoteCommand ?? (newNoteCommand = new Command(async () => await NewNote()));
            }
        }

        private Command searchTextChangedCommand;
        public Command SearchTextChangedCommand
        {
            get
            {
                return searchTextChangedCommand ?? (searchTextChangedCommand = new Command<object>((item) => SearchTextChanged(item)));
            }
        }

        public bool IsRefreshing { get; set; }

        private List<Note> noteList;
        private string filterText;

        public ObservableCollection<NoteListItem> ItemList { get; private set; }

        private NoteListItem selectedItem;
        public NoteListItem SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                selectedItem = value;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (selectedItem != null)
                    {
                        await NoteSelected(selectedItem.Note);
                    }
                });
            }
        }

        private INoteManager noteManager;
        private bool skipNextUpdateNotification;

    }
    public class NoteListItem
    {
        public Note Note { get; set; }
    }
}


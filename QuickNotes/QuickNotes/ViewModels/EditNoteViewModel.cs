﻿using Acr.UserDialogs;
using QuickNotes.Core;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QuickNotes
{
    public class EditNoteViewModel : BaseViewModel
    {

        public EditNoteViewModel()
        {
            this.noteManager = ComponentContainer.Resolve<INoteManager>();
            this.note = new Note();
            Text = "";
        }

        public EditNoteViewModel(Note note)
        {
            this.noteManager = ComponentContainer.Resolve<INoteManager>();
            this.note = note;
            Text = note.Text;
        }

        override public void OnDisappearing()
        {
            if (note.Text != null && Text.Equals(note.Text) && !note.Deleted)
            {
                return;
            }

            note.Text = Text;
            if ( (note.Id != Guid.Empty || (note.Id == Guid.Empty && Text.Length > 0)) && note.Deleted != true)
            {
                note.Dirty = true;
                noteManager.SaveNote(note);
            }

            if (note.Dirty || note.Deleted)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await noteManager.SyncNotes();
                });
            }
        }

        private async Task Done()
        {
            await Navigation.PopAsync();
        }

        private async Task DeleteNote()
        {
            bool result = await UserDialogs.Instance.ConfirmAsync(Translate("DeleteNoteText"), Translate("DeleteNoteTitle"), Translate("GenOk"), Translate("GenCancel"));
            if (result)
            {
                if (note.Id != Guid.Empty)
                {
                    await noteManager.DeleteNote(note);
                }
                else
                {
                    note.Deleted = true;
                }
                await Navigation.PopAsync();
            }
        }

        private Command deleteNoteCommand;
        public Command DeleteNoteCommand
        {
            get
            {
                return deleteNoteCommand ?? (deleteNoteCommand = new Command(async () => await DeleteNote()));
            }
        }

        private Command doneCommand;
        public Command DoneCommand
        {
            get
            {
                return doneCommand ?? (doneCommand = new Command(async () => await Done()));
            }
        }

        public string Text { get; set; }

        private INoteManager noteManager;
        private Note note;
    }
}
﻿using QuickNotes.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QuickNotes
{
    public class MenuViewModel : BaseViewModel
    {

        public MenuViewModel(MasterDetailPage masterDetailPage)
        {
            this.masterDetailPage = masterDetailPage;
        }

        private void MenuNotes()
        {
            masterDetailPage.Detail = new NavigationPage(ViewFactory.CreatePage<NoteListViewModel>());
            CloseMenu();
        }

        private void MenuAbout()
        {

            masterDetailPage.Detail = new NavigationPage(ViewFactory.CreatePage<AboutViewModel>());
            CloseMenu();
        }

        private void CloseMenu()
        {
            masterDetailPage.IsPresented = false;
        }

        private MasterDetailPage masterDetailPage;

        private Command menuNotesCommand;
        public Command MenuNotesCommand
        {
            get
            {
                return menuNotesCommand ?? (menuNotesCommand = new Command(() => MenuNotes()));
            }
        }

        private Command menuAboutCommand;
        public Command MenuAboutCommand
        {
            get
            {
                return menuAboutCommand ?? (menuAboutCommand = new Command(() => MenuAbout()));
            }
        }
    }
}

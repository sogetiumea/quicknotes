﻿using Autofac;
using Xamarin.Forms;
using QuickNotes.Core;
using System.Reflection;
using Acr.UserDialogs;

namespace QuickNotes
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            ILocalizeHelper localizeHelper = ComponentContainer.Resolve<ILocalizeHelper>();
            localizeHelper.SetLocale();

            RegisterViews();        
            SetMainPage();
        }

        public void SetMainPage()
        {
            var masterDetailPage = new MasterDetailPage
            {
                MasterBehavior = MasterBehavior.Popover
            };

            MenuViewModel menuViewModel = new MenuViewModel(masterDetailPage);

            masterDetailPage.Master = ViewFactory.CreatePage<MenuViewModel>(menuViewModel);
            masterDetailPage.Detail = new NavigationPage(ViewFactory.CreatePage<NoteListViewModel>());

            MainPage = masterDetailPage;

            if (ServicesConfig.ConnectedToTest)
            {
                UserDialogs.Instance.Alert("Connected to test server.", "Attention!", "Ok");
            }
        }

        public static void RegisterViews()
        {
            ViewFactory.Initialize();
            ViewFactory.Register<MenuViewModel, MenuPage>();
            ViewFactory.Register<AboutViewModel, AboutPage>();
            ViewFactory.Register<NoteListViewModel, NoteListPage>();
            ViewFactory.Register<EditNoteViewModel, EditNotePage>();
        }

        public static void RegisterTypes()
        {
            // Managers
            ComponentContainer.Register<INoteManager, NoteManager>(singelton: true);

            // Repositories
            ComponentContainer.Register<INoteRepository, NoteRepository>();

            // Services
            ComponentContainer.Register<INoteService, NoteService>();

            // Helpers & Utils
            ComponentContainer.Register<IDatabaseHelper, DatabaseHelper>(singelton: true);
            ComponentContainer.Register<IMessageHelper, MessageHelper>();
            ComponentContainer.Register<ITranslateHelper, TranslateHelper>();
            ComponentContainer.Register<ITimerHelper, TimerHelper>(singelton: true);
        }
    }
}

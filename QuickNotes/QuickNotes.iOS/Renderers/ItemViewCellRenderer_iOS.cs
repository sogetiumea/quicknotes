﻿using QuickNotes;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ItemViewCell) , typeof(QuickNotes.iOS.ItemViewCellRenderer_iOS))]
namespace QuickNotes.iOS
{
    class ItemViewCellRenderer_iOS : ViewCellRenderer
    {
        public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
        {
            var cell = base.GetCell(item, reusableCell, tv);
            cell.Accessory = UIKit.UITableViewCellAccessory.DisclosureIndicator;
            return cell;
        }
    }
}

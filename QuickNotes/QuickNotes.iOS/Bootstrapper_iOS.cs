﻿using QuickNotes.Core;

namespace QuickNotes.iOS
{
    public class Bootstrapper_iOS
    {
        public static void Initialize()
        {
            // Initialize the InstanceFactory
            ComponentContainer.Initialize();

            // Register common types
            App.RegisterTypes();

            // Register device specific types
            RegisterTypes();

            // Tell instance factory to build the internal container
            ComponentContainer.Build();
        }

        public static void RegisterTypes()
        {
            ComponentContainer.Register<ILocalFileSystemHelper, LocalFileSystemHelper_iOS>();
            ComponentContainer.Register<ILocalizeHelper, LocalizeHelper_iOS>();
            ComponentContainer.Register<INetworkActivityIndicatorHelper, ActivityIndicatorHelper_iOS>();
        }
    }
}

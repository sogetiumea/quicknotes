﻿using QuickNotes.Core;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;

namespace QuickNotes.iOS
{
    public class ActivityIndicatorHelper_iOS : INetworkActivityIndicatorHelper
    {
        public void Show()
        {
            activityCounter++;
            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
        }

        public void Hide()
        {
            activityCounter--;
            if (activityCounter <= 0)
            {
                activityCounter = 0;
                UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
            }
        }

        private int activityCounter = 0;
    }
}

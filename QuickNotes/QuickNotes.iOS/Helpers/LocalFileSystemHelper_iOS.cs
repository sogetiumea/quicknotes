﻿using System;
using QuickNotes.Core;
using System.Linq;
using System.Collections.Generic;

namespace QuickNotes.iOS
{
	public class LocalFileSystemHelper_iOS : ILocalFileSystemHelper
	{
		private string GetStoragePath()
		{
			string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			string libFolder = System.IO.Path.Combine(docFolder, "..", "Library");
			if (!System.IO.Directory.Exists(libFolder))
			{
				System.IO.Directory.CreateDirectory(libFolder);
			}
			return libFolder;
		}

		public string GetLocalPath(params string[] paths)
		{
			string storagePath = GetStoragePath();
			List<string> pathList = paths.ToList();
			pathList.Insert(0, storagePath);
			return System.IO.Path.Combine(pathList.ToArray());
		}

		public bool LocalPathExists(params string[] paths)
		{
			string path = GetLocalPath(paths);
			if (System.IO.Directory.Exists(path))
			{
				return true;
			}
			return System.IO.File.Exists(path);
		}

		public void CreateLocalFolder(params string[] paths)
		{
			string path = GetLocalPath(paths);
			if (!System.IO.Directory.Exists(path))
			{
				System.IO.Directory.CreateDirectory(path);
			}
		}

		public bool DeleteLocalPath(params string[] paths)
		{
			string path = GetLocalPath(paths);
			if (System.IO.Directory.Exists(path))
			{
				System.IO.Directory.Delete(path, true);
				return true;
			}
			if (System.IO.File.Exists(path))
			{
				System.IO.File.Delete(path);
				return true;
			}
			return false;
		}
			

	}
}


﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuickNotes.Core
{
    public class NoteRepository : INoteRepository
    {
        public NoteRepository(IDatabaseHelper databaseHelper)
        {
            this.db = databaseHelper;
        }

        public async Task Initialize()
        {
            await db.MainConnection.CreateTableAsync<Note>();
        }

        public async Task Clear()
        {
            await db.MainConnection.ExecuteAsync("DELETE FROM Note;");
        }

        public async Task<bool> SaveNote(Note note)
        {
            if (note.Id == Guid.Empty)
            {
                note.Id = Guid.NewGuid();
            }

            int i = await db.MainConnection.InsertOrReplaceAsync(note);
            return (i > 0);
        }

        public async Task<bool> DeleteNote(Note note)
        {
            int i = await db.MainConnection.DeleteAsync(note);
            return (i > 0);
        }

        public async Task<List<Note>> GetNotes(bool includeDeleted = false)
        {
            if (includeDeleted)
            {

                return await db.MainConnection.QueryAsync<Note>("SELECT * FROM Note ORDER BY LastChanged DESC;");
            }
            else
            {
                return await db.MainConnection.QueryAsync<Note>("SELECT * FROM Note WHERE Deleted != 1 ORDER BY LastChanged DESC;");
            }
        }

        public async Task<Note> GetNote(Guid id)
        {
            try
            {
                return await db.MainConnection.GetAsync<Note>(id);
            }
            catch
            {
                return null;
            }
        }

        private IDatabaseHelper db;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace QuickNotes.Core
{
    public sealed class TimerHelper : CancellationTokenSource, ITimerHelper
    {
        public TimerHelper()
        {
            timers = new Dictionary<string, Timer>();
        }

        public CancellationTokenSource StartTimer(Action callback, int millisecondsRepeatDelay, string timerIdentifier = null, int millisecondsStartDelay = 0)
        {
            var timer = new Timer(callback, millisecondsRepeatDelay, millisecondsStartDelay);

            if (!string.IsNullOrWhiteSpace(timerIdentifier))
            {
                if (timers.ContainsKey(timerIdentifier))
                {
                    var currentTimer = timers[timerIdentifier];
                    currentTimer.Stop();
                    timers.Remove(timerIdentifier);
                }
                timers.Add(timerIdentifier, timer);
            }

            timer.Start();
            return timer;
        }

        public bool StopTimer(string timerIdentifier)
        {
            if (timers.ContainsKey(timerIdentifier))
            {
                var currentTimer = timers[timerIdentifier];
                currentTimer.Stop();
                timers.Remove(timerIdentifier);
                return true;
            }
            return false;
        }

        private Dictionary<string, Timer> timers;
    }

    public class Timer : CancellationTokenSource
    {
        public Timer(Action callback, int millisecondsRepeatDelay, int millisecondsStartDelay)
        {
            this.callback = callback;
            this.millisecondsRepeatDelay = millisecondsRepeatDelay;
            this.millisecondsStartDelay = millisecondsStartDelay; ;
        }

        public void Start()
        {
            Task.Run(async () =>
            {
                if (millisecondsStartDelay > 0)
                {
                    await Task.Delay(millisecondsStartDelay, Token);
                }
                while (!IsCancellationRequested)
                {
                    callback.Invoke();

                    if (!IsCancellationRequested && millisecondsRepeatDelay > 0)
                    {
                        await Task.Delay(millisecondsRepeatDelay, Token);
                    }
                    else
                    {
                        Cancel();
                    }
                }
            });
        }

        public void Stop()
        {
            Cancel();
        }

        private Action callback;
        private int millisecondsRepeatDelay;
        private int millisecondsStartDelay;
    }
}

﻿//#define CONNECT_TO_TEST
#define CONNECT_TO_PROD

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickNotes.Core
{

    public class ServicesConfig
    {
#if CONNECT_TO_TEST
        public static bool ConnectedToTest = true;
        public const string WebServiceBaseAddress = "http://rest.arainyday.se/";
        public const string SaveNotesService = "save_notes.php";
        public const string GetNotesService = "get_notes.php";
#elif CONNECT_TO_PROD
        public static bool ConnectedToTest = false;
        public const string WebServiceBaseAddress = "http://rest.arainyday.se/";
        public const string SaveNotesService = "save_notes.php";
        public const string GetNotesService = "get_notes.php";
#endif

    }
}

﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickNotes.Core
{

    public class Note : IEquatable<Note>
    {
        public Note()
        {
            LastChanged = DateTime.Now;
            Id = Guid.Empty;
        }

        public override bool Equals(object obj)
        {
            Note other = obj as Note;
            if (other == null)
            {
                return false;
            }
            return (Equals(other));
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public bool Equals(Note other)
        {
            return (Id == other.Id);
        }

        [JsonIgnore]
        public bool Dirty { get; set; }

        private bool deleted;
        [JsonIgnore]
        public bool Deleted {
            get
            {
                return deleted;
            }
            set
            {
                deleted = value;
                LastChanged = DateTime.Now;
            }
         }


        [JsonProperty("guid")]
        [PrimaryKey]
        public Guid Id { get; set; }

        private string text;
        [JsonProperty("text")]
        public string Text {
            get { return text; }
            set
            {
                if (text == null || text.Equals(value) == false)
                {
                    text = value;
                    LastChanged = DateTime.Now;
                }
            }
        }

        [JsonProperty("last_changed")]
        public DateTime LastChanged { get; set; }

        [JsonIgnore]
        [Ignore]
        public string Title
        {
            get
            {
                if (Text == null)
                {
                    Text = "";
                }

                string[] lines = Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                string title;
                if (lines.Length == 0 || (title = lines[0].Trim()).Length == 0) {
                    return "(Empty note)";
                }

                return title;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace QuickNotes.Core
{
    public class NoteManager : INoteManager
    {
        public NoteManager(INoteRepository repo, INoteService service, IMessageHelper messageHelper, ITimerHelper timerHelper, INetworkActivityIndicatorHelper activityIndicatorHelper)
        {
            this.repo = repo;
            this.service = service;
            this.messageHelper = messageHelper;
            this.timerHelper = timerHelper;
            this.activityIndicatorHelper = activityIndicatorHelper;

            Task.Run(async () => {
                await Initialize();
            }).Wait();

            timerHelper.StartTimer(async () => await SyncNotes(), AppConfig.SyncTimerMilliseconds, millisecondsStartDelay: AppConfig.SyncTimerMilliseconds);
        }

        private async Task Initialize()
        {
            await repo.Initialize();
        }

        private void SendUpdateNotification()
        {
            messageHelper.Send<INoteManager>(this, Messages.NotesUpdatedMessage);
        }

        private void SyncIsFinished()
        {
            SyncingNotes = false;
            activityIndicatorHelper.Hide();
            SendUpdateNotification();
        }

        public async Task<bool> SyncNotes()
        {
            if (SyncingNotes)
            {
                return false;
            }

            SyncingNotes = true;

            activityIndicatorHelper.Show();

            List<Note> localNotes = await repo.GetNotes(includeDeleted: true);
            List<Note> serverNotes = await service.GetNotes();

            if (serverNotes == null)
            {
                SyncIsFinished();
                return false;
            }

            List<Note> syncedNotes = new List<Note>();

            foreach (var serverNote in serverNotes)
            {
                if (localNotes.Contains(serverNote))
                {
                    Note localNote = localNotes[localNotes.IndexOf(serverNote)];
                    localNotes.Remove(localNote);

                    if (localNote.Deleted && localNote.LastChanged > serverNote.LastChanged)
                    {
                        continue;
                    }
                    else if (localNote.Dirty)
                    {
                        if (localNote.LastChanged < serverNote.LastChanged)
                        {
                            localNote.Id = Guid.NewGuid();
                            syncedNotes.Add(localNote);
                            syncedNotes.Add(serverNote);
                        }
                        else
                        {
                            syncedNotes.Add(localNote);
                        }
                    }
                    else
                    {
                        syncedNotes.Add(serverNote);
                    }
                }
                else
                {
                    syncedNotes.Add(serverNote);
                }
            }

            foreach (var localNote in localNotes)
            {
                if (!serverNotes.Contains(localNote) && localNote.Dirty && !localNote.Deleted)
                {
                    syncedNotes.Add(localNote);
                }
            }

            if (!await service.SaveNotes(syncedNotes))
            {
                SyncIsFinished();
                return false;
            }

            await repo.Clear();
            foreach (Note note in syncedNotes)
            {
                note.Dirty = false;
                await repo.SaveNote(note);
            }

            SyncIsFinished();
            return true;
        }

        public async Task<List<Note>> GetNotes()
        {
            return await repo.GetNotes(false);
        }

        public async Task<Note> GetNote(Guid id)
        {
            return await repo.GetNote(id);
        }


        public async Task<bool> SaveNote(Note note)
        {
            bool result = await repo.SaveNote(note);

            if (result)
            {
                SendUpdateNotification();
            }

            return result;
        }
   
        public async Task<bool> DeleteNote(Note note)
        {
            note.Deleted = true;
            bool result = await repo.SaveNote(note);

            if (result)
            {
                SendUpdateNotification();
            }

            return result;
        }

        public bool SyncingNotes { get; private set; }

        private INoteRepository repo;
        private IMessageHelper messageHelper;
        private INoteService service;
        private ITimerHelper timerHelper;
        private INetworkActivityIndicatorHelper activityIndicatorHelper;
    }
}

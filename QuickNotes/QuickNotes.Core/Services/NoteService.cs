﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace QuickNotes.Core
{ 
    public class NoteService : BaseService, INoteService
    {
        public async Task<bool> SaveNotes(List<Note> notes)
        {
            await PostToService(ServicesConfig.SaveNotesService, notes);
            return (ResultStatusCode == HttpStatusCode.OK);
        }

        public async Task<List<Note>> GetNotes()
        {
            List<Note> noteList = await GetFromService<List<Note>>(ServicesConfig.GetNotesService);
            return noteList;
        }
    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace QuickNotes.Core
{
    public class BaseService
    {
        protected Uri GetServiceUri(string service)
        {
            return new Uri(ServicesConfig.WebServiceBaseAddress + service);
        }

        private string GetParameterString(Dictionary<string, string> parameters)
        {
            if (parameters == null || parameters.Count == 0)
            {
                return string.Empty;
            }
            var list = new List<string>();
            foreach (var item in parameters)
            {
                list.Add(item.Key + "=" + item.Value);
            }
            return "?" + string.Join("&", list);
        }

        protected async Task<T> GetFromService<T>(string service, Dictionary<string, string> parameters = null)
        {
            ResultException = null;
            ResultStatusCode = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                    HttpResponseMessage response = null;

                    string test = (GetServiceUri(service) + GetParameterString(parameters));

                    response = await client.GetAsync(GetServiceUri(service) + GetParameterString(parameters));

                    ResultStatusCode = response.StatusCode;

                    string data = null;

                    if (ResultStatusCode == HttpStatusCode.OK)
                    {
                        data = await response.Content.ReadAsStringAsync();
                        var result = await ParseJson<T>(data);
                        return result;
                    }
                    else if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Debug.WriteLine("Unauthorized");
                    }
                    else
                    {
                        Debug.WriteLine("Response code:" + response.StatusCode.ToString());
                    }

                    return default(T);
                }
            }
            catch (Exception ex)
            {
                ResultException = ex;
                Debug.WriteLine("GetFromService exception: " + ex);
                return default(T);
            }
        }

        private async Task<T> ParseJson<T>(string json)
        {
            return await Task.Run(() => {
                var deserializerSettings = new JsonSerializerSettings()
                {
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateParseHandling = DateParseHandling.DateTimeOffset,
                    NullValueHandling = NullValueHandling.Ignore
                };
                return JsonConvert.DeserializeObject<T>(json, deserializerSettings);
            });
        }
  

        protected async Task<string> GetFromService(string service, Dictionary<string, string> parameters = null)
        {
            ResultException = null;
            ResultStatusCode = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                    HttpResponseMessage response = null;

                    response = await client.GetAsync(GetServiceUri(service) + GetParameterString(parameters));

                    string test = (GetServiceUri(service) + GetParameterString(parameters));

                    ResultStatusCode = response.StatusCode;

                    string data = null;

                    if (ResultStatusCode == HttpStatusCode.OK)
                    {
                        data = await response.Content.ReadAsStringAsync();
                        return data;
                    }
                    else if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Debug.WriteLine("Unauthorized");
                    }
                    else
                    {
                        Debug.WriteLine("Response code:" + response.StatusCode.ToString());
                    }

                    return default(string);
                }
            }
            catch (Exception ex)
            {
                ResultException = ex;
                Debug.WriteLine("GetFromService exception: " + ex);
                return default(string);
            }
        }

        protected async Task<T> PostToService<T>(string service, object postObject)
        {
            ResultException = null;
            ResultStatusCode = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                    HttpResponseMessage response = null;

                    string postString = JsonConvert.SerializeObject(postObject);

                    response = await client.PostAsync(GetServiceUri(service), new StringContent(postString, Encoding.UTF8, "application/json"));

                    ResultStatusCode = response.StatusCode;

                    string data = null;

                    if (ResultStatusCode == HttpStatusCode.OK)
                    {
                        data = await response.Content.ReadAsStringAsync();
                        return await ParseJson<T>(data);
                    }
                    else if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Debug.WriteLine("Unauthorized");
                    }
                    else
                    {
                        Debug.WriteLine("Response code:" + response.StatusCode.ToString());
                    }
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                ResultException = ex;
                Debug.WriteLine("PostToService exception: " + ex);
                return default(T);
            }
        }

        protected async Task<string> PostToService(string service, object postObject)
        {
            ResultException = null;
            ResultStatusCode = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                    HttpResponseMessage response = null;

                    string postString = JsonConvert.SerializeObject(postObject);

                    response = await client.PostAsync(GetServiceUri(service), new StringContent(postString, Encoding.UTF8, "application/json"));

                    ResultStatusCode = response.StatusCode;

                    string data = null;

                    if (ResultStatusCode == HttpStatusCode.OK)
                    {
                        data = await response.Content.ReadAsStringAsync();
                        return data;
                    }
                    else if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Debug.WriteLine("Unauthorized");
                    }
                    else
                    {
                        Debug.WriteLine("Response code:" + response.StatusCode.ToString());
                    }
                    return default(string);
                }
            }
            catch (Exception ex)
            {
                ResultException = ex;
                Debug.WriteLine("PostToService exception: " + ex);
                return default(string);
            }
        }

        public async Task<byte[]> GetFile(Uri requestUri)
        {
            ResultException = null;
            ResultStatusCode = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = null;

                    response = await client.GetAsync(requestUri);

                    ResultStatusCode = response.StatusCode;

                    byte[] data = default(byte[]);

                    if (ResultStatusCode == HttpStatusCode.OK)
                    {
                        data = await response.Content.ReadAsByteArrayAsync();
                    }
                    else
                    {
                        Debug.WriteLine("Response code:" + response.StatusCode.ToString());
                    }

                    return data;
                }
            }
            catch (Exception ex)
            {
                ResultException = ex;
                Debug.WriteLine("GetFileAsync exception: " + ex);
                return default(byte[]);
            }
        }

        public HttpStatusCode? ResultStatusCode;
        public Exception ResultException;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickNotes.Core
{

public interface INoteManager
    {
        Task<List<Note>> GetNotes();
        Task<Note> GetNote(Guid id);
        Task<bool> SaveNote(Note note);
        Task<bool> DeleteNote(Note note);
        Task<bool> SyncNotes();
    }
}

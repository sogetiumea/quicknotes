﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuickNotes.Core
{
    public interface INoteRepository
    {
        Task Initialize();
        Task Clear();
        Task<bool> DeleteNote(Note note);
        Task<List<Note>> GetNotes(bool includeDeleted = false);
        Task<bool> SaveNote(Note note);
        Task<Note> GetNote(Guid Id);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuickNotes.Core
{
    public interface INoteService
    {
        Task<List<Note>> GetNotes();
        Task<bool> SaveNotes(List<Note> notes);
    }
}
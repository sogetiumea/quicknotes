﻿using SQLite;

namespace QuickNotes.Core
{
    public interface IDatabaseHelper
    {
        SQLiteAsyncConnection MainConnection { get; }

        SQLiteAsyncConnection CreateDatabase(string databaseName);
    }
}
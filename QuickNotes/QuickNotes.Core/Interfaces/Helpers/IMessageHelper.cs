﻿using System;

namespace QuickNotes.Core
{
    public class Messages
    {
        // Sent when the notes has been updated
        public const string NotesUpdatedMessage = "NotesUpdatedMessage";
    }

    public interface IMessageHelper
    {
        void Send<TSource>(TSource source, string message) where TSource : class;
        void Send<TSource, TData>(TSource source, string message, TData data) where TSource : class;
        void Subscribe<TSource>(object subscriber, string message, Action<TSource> callback) where TSource : class;
        void Subscribe<TSource, TData>(object subscriber, string message, Action<TSource, TData> callback) where TSource : class;
        void Unsubscribe<TSource>(object subscriber, string message) where TSource : class;
        void Unsubscribe<TSource, TData>(object subscriber, string message) where TSource : class;
    }
}
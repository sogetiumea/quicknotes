﻿using System;
using System.Threading;

namespace QuickNotes.Core
{
    public interface ITimerHelper
    {
        CancellationTokenSource StartTimer(Action callback, int millisecondsRepeatDelay, string timerIdentifier = null, int millisecondsStartDelay = 0);
        bool StopTimer(string timerIdentifier);
    }
}
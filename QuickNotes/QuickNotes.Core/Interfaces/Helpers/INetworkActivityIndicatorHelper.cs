﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickNotes.Core
{
    public interface INetworkActivityIndicatorHelper
    {
        void Hide();
        void Show();
    }
}

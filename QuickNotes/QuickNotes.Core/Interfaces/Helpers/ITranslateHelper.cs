﻿namespace QuickNotes.Core
{
    public interface ITranslateHelper
    {
        string GetString(string key);
    }
}
﻿
namespace QuickNotes.Core
{
	public interface ILocalFileSystemHelper
	{
		string GetLocalPath(params string[] paths);
		bool LocalPathExists(params string[] paths);
		void CreateLocalFolder(params string[] paths);
		bool DeleteLocalPath(params string[] paths);
	}
}


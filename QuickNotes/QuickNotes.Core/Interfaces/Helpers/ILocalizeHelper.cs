﻿using System.Globalization;

namespace QuickNotes.Core
{
    public interface ILocalizeHelper
    {
        CultureInfo GetCurrentCultureInfo();
        void SetLocale();
    }
}

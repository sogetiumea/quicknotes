﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms.Platform.Android;
using Acr.UserDialogs;
using System.Threading;
using System.Threading.Tasks;

namespace QuickNotes.Droid
{
    [Activity(Label = "QuickNotes", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {

            UserDialogs.Init(this);

            FormsAppCompatActivity.ToolbarResource = QuickNotes.Droid.Resource.Layout.toolbar;
            FormsAppCompatActivity.TabLayoutResource = QuickNotes.Droid.Resource.Layout.tabs;

            base.OnCreate(bundle);

            Bootstrapper_Droid.Initialize();

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
        }
    }
}


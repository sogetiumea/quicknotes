
using QuickNotes;
using QuickNotes.Core;

namespace QuickNotes.Droid
{
    class Bootstrapper_Droid
    {
        public static void Initialize()
        {
            // Initialize the InstanceFactory
            ComponentContainer.Initialize();

            // Register common types
            App.RegisterTypes();

            // Register device specific types
            RegisterTypes();

            // Tell instance factory to build the internal container
            ComponentContainer.Build();
        }

        public static void RegisterTypes()
        {
            ComponentContainer.Register<ILocalFileSystemHelper, LocalFileSystemHelper_Droid>();
            ComponentContainer.Register<ILocalizeHelper, LocalizeHelper_Droid>();
            ComponentContainer.Register<INetworkActivityIndicatorHelper, NetworkActivityIndicatorHelper_Droid>();
        }

    }
}
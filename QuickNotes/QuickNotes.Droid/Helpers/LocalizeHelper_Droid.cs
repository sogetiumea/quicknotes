using System;
using QuickNotes.Core;
using System.Threading;

namespace QuickNotes.Droid
{
    class LocalizeHelper_Droid : ILocalizeHelper
    {
        public System.Globalization.CultureInfo GetCurrentCultureInfo()
        {
            var androidLocale = Java.Util.Locale.Default;

            var netLanguage = androidLocale.ToString().Replace("_", "-");

            //Console.WriteLine("android:" + androidLocale.ToString());
            //Console.WriteLine("net:" + netLanguage);
            //Console.WriteLine(Thread.CurrentThread.CurrentCulture);
            //Console.WriteLine(Thread.CurrentThread.CurrentUICulture);

            return new System.Globalization.CultureInfo(netLanguage);
        }

        public void SetLocale()
        {
            var androidLocale = Java.Util.Locale.Default; // user's preferred locale
            var netLanguage = androidLocale.ToString().Replace("_", "-");
            var ci = new System.Globalization.CultureInfo(netLanguage);

            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
        }

    }
}